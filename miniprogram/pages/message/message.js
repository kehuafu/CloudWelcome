const app = getApp()
const db = wx.cloud.database();
const messageCollection = db.collection("student_message")
Page({

  data: {
    nameValue: '',
    numberValue: '',
    phoneValue: '',
    addressValue: '',
    parent_nameValue: '',
    parent_phoneValue: '',

    array: ['请选择', '男', '女'],
    sexValue: 0,
    btnName: '确定'
    // selectList: [{
    //   "id": "01",
    //   "text": "男"
    // }, {
    //   "id": "02",
    //   "text": "女"
    // }],
    // select: false,
  },
  onLoad: function (options) {
    console.log(options)
    var that = this
    messageCollection.where({
      _openid: wx.getStorageSync('openid')
    }).get().then(res => {
      console.log(res.data)
      if (res.data != [] && options.flag == '0') {
        wx.redirectTo({
          url: '/pages/message_success/message_success',
        })
      } else {
        if (options.flag == '1') {
          that.setData({
            nameValue: res.data[0].name,
            numberValue: res.data[0].number,
            phoneValue: res.data[0].phone,
            addressValue: res.data[0].address,
            parent_nameValue: res.data[0].parent_name,
            parent_phoneValue: res.data[0].parent_phone,
            sexValue: res.data[0].sex,
            btnName: '修改'
          })
        } else {
          that.setData({
            numberValue: wx.getStorageSync('number')
          })
        }
      }
    })
  },

  getNumberValue: function (e) {
    this.setData({
      numberValue: e.detail.value
    })
  },
  getNameValue: function (e) {
    this.setData({
      nameValue: e.detail.value
    })
  },
  getSexValue: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      sexValue: e.detail.value
    })
  },
  getPhoneValue: function (e) {
    this.setData({
      phoneValue: e.detail.value
    })
  },
  getAddressValue: function (e) {
    this.setData({
      addressValue: e.detail.value
    })
  },
  getParent_nameValue: function (e) {
    this.setData({
      parent_nameValue: e.detail.value
    })
  },
  getParent_phoneValue: function (e) {
    this.setData({
      parent_phoneValue: e.detail.value
    })
  },
  addData: function (e) {

    if (this.data.numberValue.length != 12) {
      wx.showToast({
        title: '学号有误',
        icon: 'none',
        duration: 2000
      })
      return;
    } else if (this.data.nameValue == " " || this.data.nameValue == "") {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none',
        duration: 2000
      })
      return;
    } else if (this.data.sexValue == 0) {
      wx.showToast({
        title: '请选择性别',
        icon: 'none',
        duration: 2000
      })
    } else if (this.data.phoneValue.length != 11) {
      wx.showToast({
        title: '学生号码有误',
        icon: 'none',
        duration: 2000
      })
      return;
    } else if (this.data.parent_nameValue == " " || this.data.parent_nameValue == "") {
      wx.showToast({
        title: '请输入家长姓名',
        icon: 'none',
        duration: 2000
      })
      return;
    } else if (this.data.parent_phoneValue.length != 11) {
      wx.showToast({
        title: '家长号码有误',
        icon: 'none',
        duration: 2000
      })
      return;
    } else {
      /**
       * 添加信息
       */
      if (this.data.btnName == '确定') {
        messageCollection.add({
          data: {
            number: this.data.numberValue,
            name: this.data.nameValue,
            sex: this.data.sexValue,
            phone: this.data.phoneValue,
            address: this.data.addressValue,
            parent_name: this.data.parent_nameValue,
            parent_phone: this.data.parent_phoneValue
          },
          success: res => {
            console.log(res)
            wx.showToast({
              title: '录入成功',
              icon: 'none',
              duration: 2000
            })
            wx.redirectTo({
              url: '../message_success/message_success',
            })
          },
        })
      } else {
        messageCollection.where({
          _openid: wx.getStorageSync('openid')
        }).update({
          data: {
            number: this.data.numberValue,
            name: this.data.nameValue,
            sex: this.data.sexValue,
            phone: this.data.phoneValue,
            address: this.data.addressValue,
            parent_name: this.data.parent_nameValue,
            parent_phone: this.data.parent_phoneValue
          }
        }).then(res => {
          wx.showToast({
            title: '修改成功',
            icon: 'none',
            duration: 2000
          })
          wx.redirectTo({
            url: '../message_success/message_success',
          })
        })
      }
    }
  }
})