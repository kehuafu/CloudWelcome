const db = wx.cloud.database()
const student_messageCollection = db.collection("student_message")

Page({
  onLoad: function (options) {
    student_messageCollection.where({
      _openid: wx.getStorageSync('openid')
    }).get().then(res => {
      this.setData({
        student: res.data
      })
    })
  },
  /**
   * 确认信息
   */
  OnTap: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 修改信息
   * @param {}} ev 
   */
  OnChange(ev) {
    wx.redirectTo({
      url: '/pages/message/message?flag=1'
    })
  }
})