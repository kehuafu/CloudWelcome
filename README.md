# 云迎新

- **项目介绍**

  “云迎新”是为了方便学校新学生及其家长前来报道时，快速了解校园环境的服务型小程序。

- **应用场景**

  学校迎新工作

- **目标用户**

  学校新学生（需登录）和普通用户

- **实现思路**

  采用微信小程序通过云函数连接登录模块的云数据库，从而验证新学生身份（***目前非真实数据，仅模拟数据***）；校园风景模块采用云数据库以及云存储相关数据，解决图片加载缓慢的问题；导航指引模块采用云数据库储存相关坐标位置信息，从而实现在腾讯地图上对学校区域的划分；数据分析模块采用云数据库存储各学院人数等相关数据（***目前均为模拟数据***）。

- **架构图**

    <img src="/images/0.png" style="zoom:60%;" />

- **项目效果截图**

  - （1）启动页面

    ​	<img src="/images/1.jpg" style="zoom:25%;" />

  - （2）主页面

    ​	<img src="/images/2.jpg" style="zoom:25%;" />

  - （3）登录页面

    ​	<img src="/images/3.jpg" style="zoom:25%;" />

  - （4）信息采集页面

    ​	<img src="/images/7.jpg" style="zoom:65%;" />

  - （5）导航指引页面

    ​	<img src="/images/4.jpg" style="zoom:25%;" />

  - （6）校园风景页面

  ​	     <img src="/images/5.jpg" style="zoom:25%;" />

  - （7）数据分析页面

    ​	<img src="/images/6.jpg" style="zoom:25%;" />

- **小程序体验码（已上线）**

  ​	<img src="/images/8.jpg" style="zoom:80%;" />

- **部署教程**

  - 下载代码

    ```nginx
    git clone https://gitee.com/kehuafu/CloudWelcome.git
    ```

  - 将代码导入到[微信开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)，并修改AppID参数

      ![](/images/9.jpg)

  - 需要部署login.js云函数

    ```js
    // 云函数模板
    // 部署：在 cloud-functions/login 文件夹右击选择 “上传并部署”
    
    const cloud = require('wx-server-sdk')
    
    // 初始化 cloud
    cloud.init({
      // API 调用都保持和云函数当前所在环境一致
      env: cloud.DYNAMIC_CURRENT_ENV
    })
    
    /**
     * 这个示例将经自动鉴权过的小程序用户 openid 返回给小程序端
     * 
     * event 参数包含小程序端调用传入的 data
     * 
     */
    exports.main = async (event, context) => {
      console.log(event)
      console.log(context)
    
      // 可执行其他自定义逻辑
      // console.log 的内容可以在云开发云函数调用日志查看
    
      // 获取 WX Context (微信调用上下文)，包括 OPENID、APPID、及 UNIONID（需满足 UNIONID 获取条件）等信息
      const wxContext = cloud.getWXContext()
    
      return {
        event,
        openid: wxContext.OPENID,
        appid: wxContext.APPID,
        unionid: wxContext.UNIONID,
        env: wxContext.ENV,
      }
    }
    ```

  - 云数据库中需要创建以下表

      ![](/images/10.png)

  - 云存储中需要上传以下文件用来存储风景图片

     ![image-20200918224401453](/images/image-20200918224401453.png)

  - 后台需要配置的插件服务，插件的使用方法，请参考官方文档

     <img src="/images/11.png" style="zoom:80%;" />

- **开源许可证标注**

  #### [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)

- **参考文档**
  
  - [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

